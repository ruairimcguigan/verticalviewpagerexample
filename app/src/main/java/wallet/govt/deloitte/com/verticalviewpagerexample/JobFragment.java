package wallet.govt.deloitte.com.verticalviewpagerexample;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class JobFragment extends Fragment {


    public JobFragment() {
        // Required empty public constructor
    }

    public static final JobFragment newInstance() {
        JobFragment f = new JobFragment();
        Bundle bdl = new Bundle(1);
        f.setArguments(bdl);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_job, container, false);
    }


}
