package wallet.govt.deloitte.com.verticalviewpagerexample;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;

public class MainActivity extends FragmentActivity {

        @Override

        public void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);

            VerticalViewPager pager = ( VerticalViewPager ) findViewById(R.id.viewpager);

            pager.setAdapter(new MyPageAdapter(getSupportFragmentManager()));
        }


    public class MyPageAdapter extends FragmentPagerAdapter {
        public MyPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch ( position ) {
                case 0:
                    return NameFragment.newInstance();
                case 1:
                    return AgeFragment.newInstance();
                case 2:
                    return JobFragment.newInstance();
                default:
                    return NameFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}

